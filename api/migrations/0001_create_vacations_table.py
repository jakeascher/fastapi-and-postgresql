steps = [
    (
        """
        CREATE TABLE vacations (
            id SERIAL PRIMARY KEY,
            name VARCHAR(100) NOT NULL,
            start_date DATE NOT NULL,
            end_date DATE NOT NULL,
            thoughts TEXT
        );
        """,
        """
        DROP TABLE vacations;
        """,
    )
]
