steps = [
    (
        """CREATE TABLE users (
            id SERIAL PRIMARY KEY,
            username VARCHAR(50) NOT NULL UNIQUE,
            email VARCHAR(50) NOT NULL UNIQUE,
            full_name VARCHAR(50) NOT NULL,
            disabled BOOLEAN NOT NULL,
            hashed_password VARCHAR(100) NOT NULL
        );""",
        """DROP TABLE users;""",
    ),
]
