from fastapi import FastAPI

from routers import vacations, users

app = FastAPI()
app.include_router(vacations.router)
app.include_router(users.router)
