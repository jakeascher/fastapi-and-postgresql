from typing import Annotated

from fastapi import APIRouter, Depends, Response

from queries.vacations import VacationIn, VacationOut, Error, VacationRepository
from queries.users import oauth2_scheme

router = APIRouter()

@router.post("/vacations", response_model=VacationOut | Error)
def create_vacation(
    token: Annotated[str, Depends(oauth2_scheme)],
    vacation: VacationIn,
    repo: VacationRepository = Depends()
) -> VacationOut | Error:
    return repo.create(vacation)

@router.get("/vacations", response_model=list[VacationOut] | Error)
def get_vacations(
    token: Annotated[str, Depends(oauth2_scheme)],
    response: Response,
    repo: VacationRepository = Depends()
) -> list[VacationOut]:
    result = repo.get_all()
    if not isinstance(result, list):
        response.status_code = 500
    return result

@router.get("/vacations/{id}", response_model=VacationOut | Error)
def get_vacation(
    id: int,
    response: Response,
    repo: VacationRepository = Depends()
) -> VacationOut | Error:
    result = repo.get(id)
    if result is None:
        response.status_code = 404
        return {"message": "Vacation not found"}
    return result

@router.delete("/vacations/{id}", response_model=VacationOut | Error)
def delete_vacation(
    id: int,
    response: Response,
    repo: VacationRepository = Depends()
) -> VacationOut | Error:
    result = repo.delete(id)
    if result is None:
        response.status_code = 404
        return {"message": "Vacation not found"}
    return result

@router.put("/vacations/{id}", response_model=VacationOut | Error)
def update_vacation(
    id: int,
    vacation: VacationIn,
    response: Response,
    repo: VacationRepository = Depends()
) -> VacationOut | Error:
    result = repo.update(id, vacation)
    if result is None:
        response.status_code = 404
        return {"message": "Vacation not found"}
    return result
