from datetime import timedelta
from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from queries.users import (
    UserRepository,
    UserIn,
    UserOut,
    Token,
    get_current_active_user,
)

router = APIRouter()

ACCESS_TOKEN_EXPIRE_MINUTES = 30

@router.get("/users/me", response_model=UserOut)
async def read_users_me(
    current_user: Annotated[UserOut, Depends(get_current_active_user)],
):
    return current_user

@router.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    repo: UserRepository = Depends()
) -> Token:
    user = repo.authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = repo.create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

@router.post("/users/signup", response_model=UserOut)
async def signup(user: UserIn, repo: UserRepository = Depends()) -> UserOut:
    # check if username is already registered
    if repo.get_by_username(user.username):
        raise HTTPException(status_code=400, detail="Username already registered")
    # check if email is already registered
    if repo.get_by_email(user.email):
        raise HTTPException(status_code=400, detail="Email already registered")
    # check if passwords match
    if user.password != user.password_confirmation:
        raise HTTPException(status_code=400, detail="Passwords do not match")
    return repo.create(user)
