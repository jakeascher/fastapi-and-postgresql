import os
from datetime import datetime, timedelta
from typing import Annotated

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from pydantic import BaseModel
from passlib.context import CryptContext
from jose import JWTError, jwt

from queries.pool import pool

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

class UserIn(BaseModel):
    username: str
    password: str
    password_confirmation: str
    email: str | None = None
    full_name: str | None = None

class UserOut(BaseModel):
    id: int
    username: str
    email: str | None = None
    full_name: str | None = None
    disabled: bool| None = None

class UserInDB(UserOut):
    hashed_password: str

class Error(BaseModel):
    message: str

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: str | None = None

class UserRepository:
    def __init__(self) -> None:
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def create_access_token(self, data: dict, expires_delta: datetime | None = None) -> str:
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=15)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, os.environ["SIGNING_KEY"], algorithm=os.environ["HASHING_ALGORITHM"])
        return encoded_jwt

    def verify_password(self, plain_password: str, hashed_password: str) -> bool:
        return self.pwd_context.verify(plain_password, hashed_password)

    def authenticate_user(self, username: str, password: str) -> UserInDB | None:
        user = self.get_by_username(username)
        if not user:
            return None
        if not self.verify_password(password, user.hashed_password):
            return None
        return user

    def create(self, user: UserIn) -> UserOut | Error:
        hashed_password = self.pwd_context.hash(user.password)

        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users (username, email, full_name, disabled, hashed_password)
                        VALUES (%s, %s, %s, %s, %s)
                        RETURNING id, username, email, full_name, disabled;
                        """,
                        (user.username, user.email, user.full_name, False, hashed_password),
                    )
                    row = result.fetchone()
                    return self.record_to_user_out(row)
        except Exception:
            return {"message": "Create did not work"}

    def get_all(self) -> list[UserOut] | Error:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, email, full_name, disabled, hashed_password
                        FROM users
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_user_out(row)
                        for row in result
                    ]
        except Exception:
            return {"message": "Get all did not work"}

    def get(self, id: int) -> UserOut | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, email, full_name, disabled
                        FROM users
                        WHERE id = %s;
                        """,
                        (id,),
                    )
                    row = result.fetchone()
                    if row is None:
                        return None
                    return self.record_to_user_out(row)
        except Exception:
            return {"message": "Get did not work"}

    def get_by_username(self, username: str) -> UserInDB | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, email, full_name, disabled, hashed_password
                        FROM users
                        WHERE username = %s;
                        """,
                        (username,),
                    )
                    row = result.fetchone()
                    if row is None:
                        return None
                    return UserInDB(
                        id=row[0],
                        username=row[1],
                        email=row[2],
                        full_name=row[3],
                        disabled=row[4],
                        hashed_password=row[5],
                    )
        except Exception:
            return {"message": "Get by username did not work"}

    def get_by_email(self, email: str) -> UserOut | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, email, full_name, disabled
                        FROM users
                        WHERE email = %s;
                        """,
                        (email,),
                    )
                    row = result.fetchone()
                    if row is None:
                        return None
                    return self.record_to_user_out(row)
        except Exception:
            return {"message": "Get by email did not work"}

    def record_to_user_out(self, row) -> UserOut:
        return UserOut(
            id=row[0],
            username=row[1],
            email=row[2],
            full_name=row[3],
            disabled=row[4],
        )

def get_current_user(
    token: Annotated[str, Depends(oauth2_scheme)],
) -> UserOut:
    repo = UserRepository()
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, os.environ["SIGNING_KEY"], algorithms=[os.environ["HASHING_ALGORITHM"]])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError as e:
        raise credentials_exception
    user = repo.get_by_username(username=token_data.username)
    if user is None:
        raise credentials_exception
    return UserOut(
        id=user.id,
        username=user.username,
        email=user.email,
        full_name=user.full_name,
        disabled=user.disabled,
    )

def get_current_active_user(current_user: Annotated[UserOut, Depends(get_current_user)]) -> UserOut:
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user
