from datetime import date
from pydantic import BaseModel

from queries.pool import pool

class VacationIn(BaseModel):
    name: str
    start_date: date
    end_date: date
    thoughts: str | None

class VacationOut(BaseModel):
    id: int
    name: str
    start_date: date
    end_date: date
    thoughts: str | None

class Error(BaseModel):
    message: str

class VacationRepository:
    def create(self, vacation: VacationIn) -> VacationOut | Error:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO vacations (name, start_date, end_date, thoughts)
                        VALUES (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        (vacation.name, vacation.start_date, vacation.end_date, vacation.thoughts),
                    )
                    id = result.fetchone()[0]
                    old_data = vacation.dict()
                    return VacationOut(id=id, **old_data)
        except Exception:
            return {"message": "Create did not work"}

    def get_all(self) -> list[VacationOut] | Error:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name, start_date, end_date, thoughts
                        FROM vacations
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_vacation_out(row)
                        for row in result
                    ]
        except Exception:
            return {"message": "Get all did not work"}

    def get(self, id: int) -> VacationOut | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name, start_date, end_date, thoughts
                        FROM vacations
                        WHERE id = %s;
                        """,
                        (id,),
                    )
                    row = result.fetchone()
                    if row is None:
                        return None
                    return self.record_to_vacation_out(row)
        except Exception:
            return {"message": "Get did not work"}

    def delete(self, id: int) -> VacationOut | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM vacations
                        WHERE id = %s
                        RETURNING id, name, start_date, end_date, thoughts;
                        """,
                        (id,),
                    )
                    row = result.fetchone()
                    if row is None:
                        return None
                    return self.record_to_vacation_out(row)
        except Exception:
            return {"message": "Delete did not work"}

    def update(self, id: int, vacation: VacationIn) -> VacationOut | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE vacations
                        SET name = %s, start_date = %s, end_date = %s, thoughts = %s
                        WHERE id = %s
                        RETURNING id, name, start_date, end_date, thoughts;
                        """,
                        (vacation.name, vacation.start_date, vacation.end_date, vacation.thoughts, id),
                    )
                    row = result.fetchone()
                    if row is None:
                        return None
                    return self.record_to_vacation_out(row)
        except Exception:
            return {"message": "Update did not work"}

    def record_to_vacation_out(self, row) -> VacationOut:
        return VacationOut(
            id=row[0],
            name=row[1],
            start_date=row[2],
            end_date=row[3],
            thoughts=row[4],
        )
